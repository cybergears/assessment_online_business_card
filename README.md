Instructions for installation of the webapp.

Basic Info: 
#Code By: Shahrukh Sheikh
#language Used: HTML,CSS,JQuery, PHP,Mysql
#Programming Approach: Procedural Programming ( No PHP Frameworks used )

--

Directory Structure of important files:
-api - (Holds all the data parsing files written in JS)
-config - (Holds database link)
-inc - (Holds all the file required to feed the database and the webapp)
-install - (Holds all the info regarding database schema) 
-style - (Holds all all custom stylings)
-themes - (Holds all the themes used in frontend)
-uploads - (holds all the profile pictures and gallery images)
-user - (holds user webapp)
-admin - (holds admin webapp) .


Website Installation Procedure:

1. Upload the website to the server.
2. navigate to the install folder Eg. www.xyz.com/install
Fill your database login details and database name to be used by the webapp. Hit install button.

Incomplete Modules Information:




NOTE: the pritty URL semantics are tested on apache server and are working fine but as far as NGINX is concerned it 
will not work as URL rewrite engine of .htaccess file does not supports.

To use it on nginx use this url rewrite rule:

server {
    server_name example.com;
    location ~ "^/(.*/([a-zA-Z0-9_-]+)|([a-zA-Z0-9_-]+))$" {
        try_files $uri /./index.php?id=$1;
    }
}

* Change the example.com with your desigred url.
admin credentials: 
username: admin
password: admin
login url: {server_url}/adminLogin.php

In order to view profile like www.weburl.com/username 
Edit: userList.php on line 22 replace "$data .= '<a href="index.php?id='.$username.'">" with "$data .= '<a href="'.$username.'">"

NOTE: as the application is designed to re create the portfolio image during upload so, if you get error/blank screen during portfolio update make sure that PHP GD library with libjpeg support is installed outerwise portfolio update will not work.  



